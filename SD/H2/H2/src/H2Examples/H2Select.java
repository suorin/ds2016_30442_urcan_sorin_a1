package H2Examples;
import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import classes.Student;

public class H2Select {
	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/h2schema";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "root";
	   
	   @Test
	   public void select() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   Student student = new Student();
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Select...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, name, birthdate, address FROM student WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, 0);
		      
		      ResultSet rs = pstmt.executeQuery();

		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         String name = rs.getString("name");
		         String birthdate = rs.getString("birthdate");
		         String address = rs.getString("address");
		         
		         assertEquals(0, id);
		         assertEquals("John Smith", name);
		         assertEquals("4 feb 1993", birthdate);
		         assertEquals("Str. Baritiu", address);
		         
		         student.setName(name);
		         student.setAddress(address);
		         student.setBirthdate(birthdate);
		         student.setId(id);

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", name: " + name);
		         System.out.print(", birthdate: " + birthdate);
		         System.out.println(", address: " + address);
		      }
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   
	   }
	   
}
