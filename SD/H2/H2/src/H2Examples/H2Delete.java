package H2Examples;
import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

public class H2Delete {
	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/h2schema";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "root";

	   @Test
	   public void delete() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Delete...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "delete from Student where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setInt (1, 1);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   @Test
	   public void select() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, name, birthdate, address FROM student WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, 1);
		      
		      ResultSet rs = pstmt.executeQuery();

		      //STEP 5: Extract data from result set
		      int size = 0;
		      while(rs.next()){
		         //Retrieve by column name
		         size++;
		      }
		      assertEquals(0, size);
		      
		      
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }
}
