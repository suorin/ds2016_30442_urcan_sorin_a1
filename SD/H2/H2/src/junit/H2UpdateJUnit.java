package junit;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import H2Examples.H2Update;

public class H2UpdateJUnit {
    public static void main(String[] args) {
	   	   Result result = JUnitCore.runClasses(H2Update.class);
			
		      for (Failure failure : result.getFailures()) {
		         System.out.println(failure.toString());
		      }
				
		      System.out.println(result.wasSuccessful());
	       }
}
