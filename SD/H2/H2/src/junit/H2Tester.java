package junit;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import classes.H2Start;

public class H2Tester {      
	      public static void main(String[] args) {
	   	   Result result = JUnitCore.runClasses(H2Start.class);
			
		      for (Failure failure : result.getFailures()) {
		         System.out.println(failure.toString());
		      }
				
		      System.out.println(result.wasSuccessful());
	       }
}
