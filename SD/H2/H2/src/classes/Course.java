package classes;

public class Course {
	int id;
	String name;
	String teacher;
	int studyyear;
	
	public Course(int id, String name, String teacher, int studyyear) {
		this.id = id;
		this.name = name;
		this.teacher = teacher;
		this.studyyear = studyyear;
	}
	
	public Course() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public int getStudyyear() {
		return studyyear;
	}
	public void setStudyyear(int studyyear) {
		this.studyyear = studyyear;
	}
}
