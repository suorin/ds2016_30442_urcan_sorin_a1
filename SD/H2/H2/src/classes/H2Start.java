package classes;
import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

public class H2Start {
	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/h2schema";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "root";
	   
	   
	   
	   public static void main(String[] args) {
		   //select(0); 
		   //insert(1, "George", "4 feb 1990", "Str. D");
		   //update(1, "UGeorge", "U4 feb 1990", "UStr. D");
		   //delete(1);
		   
		   //cSelect(0);
		   //cInsert(4, "CName", "CTeacher", 4);
		   //cUpdate(4, "UCName", "UCTeacher", 5);
		   //cDelete(4);
		   
		   viewAttendants("Math");
		   System.out.println("IT: ");
		   viewAttendants("IT");
	   }  
	   
	   public static Student select(int wantedId) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   Student student = new Student();
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Select...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, name, birthdate, address FROM student WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, wantedId);
		      
		      ResultSet rs = pstmt.executeQuery();

		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         String name = rs.getString("name");
		         String birthdate = rs.getString("birthdate");
		         String address = rs.getString("address");
		         
		         assertEquals(0, id);
		         assertEquals("John Smith", name);
		         assertEquals("4 feb 1993", birthdate);
		         assertEquals("Str. Baritiu", address);
		         
		         student.setName(name);
		         student.setAddress(address);
		         student.setBirthdate(birthdate);
		         student.setId(id);

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", name: " + name);
		         System.out.print(", birthdate: " + birthdate);
		         System.out.println(", address: " + address);
		      }
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   
		   return student;
	   }
	   
	   public static void insert(int id, String name, String birthdate, String address) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Insert...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "insert into Student (id, name, birthdate, address)" + " values (?, ?, ?, ?)";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt (1, 1);
		      pstmt.setString (2, name);
		      pstmt.setString   (3, birthdate);
		      pstmt.setString(4, address);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   public static void update(int id, String n, String b, String a) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Update...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "update Student set name = ?, birthdate = ?, address = ? where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setString (1, n);
		      pstmt.setString (2, b);
		      pstmt.setString(3, a);
		      pstmt.setInt (4, id);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.executeUpdate();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   public static void delete(int id) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Delete...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "delete from Student where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setInt (1, id);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   public static Course cSelect(int wantedId) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   Course course = new Course();
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Select...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, name, teacher, studyyear FROM Courses WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, wantedId);
		      
		      ResultSet rs = pstmt.executeQuery();

		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         String name = rs.getString("name");
		         String teacher = rs.getString("teacher");
		         int studyyear = rs.getInt("studyyear");
		         
		         course.setName(name);
		         course.setTeacher(teacher);
		         course.setStudyyear(studyyear);
		         course.setId(id);

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", name: " + name);
		         System.out.print(", teacher: " + teacher);
		         System.out.println(", studyyear: " + studyyear);
		      }
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   
		   return course;
	   }
	   
	   public static void cInsert(int id, String n, String t, int s) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Insert...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "insert into Courses (id, name, teacher, studyyear)" + " values (?, ?, ?, ?)";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt (1, id);
		      pstmt.setString (2, n);
		      pstmt.setString   (3, t);
		      pstmt.setInt(4, s);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   public static void cUpdate(int id, String n, String t, int s) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Update...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "update Courses set name = ?, teacher = ?, studyyear = ? where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setString (1, n);
		      pstmt.setString (2, t);
		      pstmt.setInt(3, s);
		      pstmt.setInt (4, id);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.executeUpdate();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   public static void cDelete(int id) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Delete...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "delete from Courses where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setInt (1, id);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }

	   public static void viewAttendants(String courseName) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   Course course = new Course();
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, name, teacher, studyyear FROM Courses WHERE name = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setString(1, courseName);
		      
		      ResultSet rs = pstmt.executeQuery();

		      //STEP 5: Extract data from result set
		      int courseId = -1;
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         String name = rs.getString("name");
		         String teacher = rs.getString("teacher");
		         int studyyear = rs.getInt("studyyear");
		         
		         course.setName(name);
		         course.setTeacher(teacher);
		         course.setStudyyear(studyyear);
		         course.setId(id);
		         
		         courseId = id;
		      }
		      
		      String sql0;
		      sql0 = "SELECT studentId FROM enroll WHERE courseId = ?";
		      PreparedStatement pstmt0 = null;
		      pstmt0 = conn.prepareStatement(sql0);
		      pstmt0.setInt(1, courseId);
		      ResultSet rs0 = pstmt.executeQuery();
		      
		      rs0 = pstmt0.executeQuery();
		      while(rs0.next()){
			         //Retrieve by column name
			         int studentId  = rs0.getInt("studentId");
			         
			         String sql1;
				      sql1 = "SELECT name FROM Student WHERE id = ?";
				      PreparedStatement pstmt1 = null;
				      pstmt1 = conn.prepareStatement(sql1);
				      pstmt1.setInt(1, studentId);
				      ResultSet rs1 = pstmt1.executeQuery();
				      while(rs1.next()){
					         String name = rs1.getString("name");			         
					         System.out.println(name + " ");
					      }
			      }
		      
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }
	   




	   //cod op : Insert - 0, Update - 1, Delete - 2
	   public void selectSpecial(int operatie) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, first, last, age FROM Employees WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, 1);
		      
		      ResultSet rs = pstmt.executeQuery();

		      int size = 0;
		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         int age = rs.getInt("age");
		         String first = rs.getString("first");
		         String last = rs.getString("last");
		         
		         if (operatie == 1) {
		        	 assertEquals(1, id);
		        	 assertEquals(23, age);
		        	 assertEquals("Help", first);
		        	 assertEquals("Fast", last); }
		         else if (operatie == 2) {
		        	 assertEquals(1, id);
		        	 assertEquals(24, age);
		        	 assertEquals("UHelp", first);
		        	 assertEquals("UFast", last);
		         } else if (operatie == 3) {
		        	 size++;
			      }

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", Age: " + age);
		         System.out.print(", First: " + first);
		         System.out.println(", Last: " + last);
		      }
		      
		      if (operatie == 3) {
			      assertEquals(0, size);
			  }
		      
		      
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }
}
