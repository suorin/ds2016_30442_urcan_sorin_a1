package classes;

public class Student {
	int id;
	String name;
	String birthdate;
	String address;
	
	public Student (int id, String name, String birthdate, String address) {
		this.id = id;
		this.name = name;
		this.birthdate = birthdate;
		this.address = address;
	}
	
	public Student() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
