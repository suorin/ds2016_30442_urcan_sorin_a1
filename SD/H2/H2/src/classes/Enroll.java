package classes;

public class Enroll {
	int id;
	int studentId;
	int courseId;
	
	public Enroll(int id, int studentId, int courseId) {
		super();
		this.id = id;
		this.studentId = studentId;
		this.courseId = courseId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public int getCourseId() {
		return courseId;
	}

	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}
	
	
}
