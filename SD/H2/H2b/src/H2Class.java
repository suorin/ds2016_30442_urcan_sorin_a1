import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

public class H2Class {
	// JDBC driver name and database URL
	   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost/h2schema";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "root";
	   
	   
	   /*
	   public static void main(String[] args) {
		   select(); 
		   insert();
		   update();
		   delete();
	   }  */
	   
	   @Test
	   public void wselect() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Select...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, first, last, age FROM Employees WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, 0);
		      
		      ResultSet rs = pstmt.executeQuery();

		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         int age = rs.getInt("age");
		         String first = rs.getString("first");
		         String last = rs.getString("last");
		         
		         assertEquals(0, id);
		         assertEquals(22, age);
		         assertEquals("John", first);
		         assertEquals("Smith", last);

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", Age: " + age);
		         System.out.print(", First: " + first);
		         System.out.println(", Last: " + last);
		      }
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }
	   
	   @Test
	   public void xinsert() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Insert...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "insert into Employees (id, age, first, last)"
        + " values (?, ?, ?, ?)";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt (1, 1);
		      pstmt.setInt (2, 23);
		      pstmt.setString   (3, "Help");
		      pstmt.setString(4, "Fast");
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   
		   selectSpecial(1);
	   }

	   @Test
	   public void yupdate() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Update...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "update Employees set age = ?, first = ?, last = ? where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setInt (1, 24);
		      pstmt.setString (2, "UHelp");
		      pstmt.setString(3, "UFast");
		      pstmt.setInt (4, 1);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.executeUpdate();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   
		   selectSpecial(2);
	   }

	   @Test
	   public void zdelete() {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Delete...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "delete from Employees where id = ?";

		      pstmt = conn.prepareStatement(sql);
		      
		      pstmt.setInt (1, 1);
		      
		      //ResultSet rs = pstmt.executeQuery();
		      pstmt.execute();
		      
		      //STEP 6: Clean-up environment
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   
		   selectSpecial(3);
	   }







	   //cod op : Insert - 0, Update - 1, Delete - 2
	   public void selectSpecial(int operatie) {
		   Connection conn = null;
		   PreparedStatement pstmt = null;
		   
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      		   
		      //STEP 4: Execute a query
		      String sql;
		      sql = "SELECT id, first, last, age FROM Employees WHERE id = ?";

		      pstmt = conn.prepareStatement(sql);
		      pstmt.setInt(1, 1);
		      
		      ResultSet rs = pstmt.executeQuery();

		      int size = 0;
		      //STEP 5: Extract data from result set
		      while(rs.next()){
		         //Retrieve by column name
		         int id  = rs.getInt("id");
		         int age = rs.getInt("age");
		         String first = rs.getString("first");
		         String last = rs.getString("last");
		         
		         if (operatie == 1) {
		        	 assertEquals(1, id);
		        	 assertEquals(23, age);
		        	 assertEquals("Help", first);
		        	 assertEquals("Fast", last); }
		         else if (operatie == 2) {
		        	 assertEquals(1, id);
		        	 assertEquals(24, age);
		        	 assertEquals("UHelp", first);
		        	 assertEquals("UFast", last);
		         } else if (operatie == 3) {
		        	 size++;
			      }

		         //Display values
		         System.out.print("ID: " + id);
		         System.out.print(", Age: " + age);
		         System.out.print(", First: " + first);
		         System.out.println(", Last: " + last);
		      }
		      
		      if (operatie == 3) {
			      assertEquals(0, size);
			  }
		      
		      
		      //STEP 6: Clean-up environment
		      rs.close();
		      pstmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
			   if (pstmt != null) {
				   try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				}
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
	   }
}
