/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author friends
 */

public class User {

    private static final String findUser =
            "select * from Users where username = ? and pass = ?";

    private static final String insertUserStatement =
            "insert into Users(username, pass) "+ "values(?, ?)";

    private int id;
    private String username;
    private String pass;

     public static User findUser(String username, String pass) throws Exception{
        Database db = Database.getInstace();
        PreparedStatement stmt = db.prepareStatement(findUser);
        stmt.setString(1, username);
        stmt.setString(2, pass);
        ResultSet rs = stmt.executeQuery();
        if(rs.next()){
            int id = rs.getInt("id");
            return new User(id, username, pass);
        }else{
            return null;
        }
    }

    public User(){
        id = -1;
        username = "";
        pass = "";
    }

    public User(int id, String username, String pass){
        this.id = id;
        this.username = username;
        this.pass = pass;
    }

    public User(String username, String pass){
        this.id = -1;
        this.username = username;
        this.pass = pass;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass the pass to set
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean add(){
        try{
            Database db = Database.getInstace();
            PreparedStatement stmt = db.prepareStatement(insertUserStatement);
            stmt.setString(1, username);
            stmt.setString(2, pass);
            stmt.executeUpdate();
            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }
}

