/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package data;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

/**
 *
 * @author friends
 */
public class Database {

    private static Database instance;

    private Connection con;

    public static Database getInstace() throws Exception{
        if(instance == null)
            instance = new Database();
        return instance;
    }

    private Database() throws Exception{
        Class.forName("com.mysql.jdbc.Driver");
        
        String DB_URL = "jdbc:mysql://localhost:3306/onlinetrackingsystemdb?zeroDateTimeBehavior=convertToNull";
        Properties properties = new Properties();
        properties.setProperty("user", "root");
        properties.setProperty("password", "root");
        properties.setProperty("useSSL", "false");
        properties.setProperty("autoReconnect", "true");
        con = DriverManager.getConnection(DB_URL, properties);
        
    }


    public ResultSet executeQuery(String query) throws Exception{
        Statement stmt = con.createStatement();
        return stmt.executeQuery(query);
    }

    public PreparedStatement prepareStatement(String query) throws Exception{
        return con.prepareStatement(query);
    }

}


