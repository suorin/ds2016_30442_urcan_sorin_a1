/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package webservice;

import data.User;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author friends
 */

@WebService()
public class UserWS {

    @WebMethod
    /**
     * returns true if the user have been registered successfully
     */
    public boolean registerUser(String username, String pass){
        User usr = new User(username, pass);
        return usr.add();
    }

    @WebMethod
    /**
     * returns true if the login is correct
     */
    public boolean loginUser(String username, String pass){
        User usr = null;
        try {
            usr = User.findUser(username, pass);
        } catch (Exception ex) {}
        if(usr != null)
            return true;
        else
            return false;
    }
}

