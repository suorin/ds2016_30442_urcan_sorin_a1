
package packagesystem2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UpdatePackageStatusResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "updatePackageStatusResult"
})
@XmlRootElement(name = "UpdatePackageStatusResponse")
public class UpdatePackageStatusResponse {

    @XmlElement(name = "UpdatePackageStatusResult")
    protected boolean updatePackageStatusResult;

    /**
     * Gets the value of the updatePackageStatusResult property.
     * 
     */
    public boolean isUpdatePackageStatusResult() {
        return updatePackageStatusResult;
    }

    /**
     * Sets the value of the updatePackageStatusResult property.
     * 
     */
    public void setUpdatePackageStatusResult(boolean value) {
        this.updatePackageStatusResult = value;
    }

}
