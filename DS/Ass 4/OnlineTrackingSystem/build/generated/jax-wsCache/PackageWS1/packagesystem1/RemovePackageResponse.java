
package packagesystem1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemovePackageResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "removePackageResult"
})
@XmlRootElement(name = "RemovePackageResponse")
public class RemovePackageResponse {

    @XmlElement(name = "RemovePackageResult")
    protected boolean removePackageResult;

    /**
     * Gets the value of the removePackageResult property.
     * 
     */
    public boolean isRemovePackageResult() {
        return removePackageResult;
    }

    /**
     * Sets the value of the removePackageResult property.
     * 
     */
    public void setRemovePackageResult(boolean value) {
        this.removePackageResult = value;
    }

}
