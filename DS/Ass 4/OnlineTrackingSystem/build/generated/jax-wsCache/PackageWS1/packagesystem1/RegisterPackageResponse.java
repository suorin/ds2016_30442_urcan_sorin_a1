
package packagesystem1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RegisterPackageResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "registerPackageResult"
})
@XmlRootElement(name = "RegisterPackageResponse")
public class RegisterPackageResponse {

    @XmlElement(name = "RegisterPackageResult")
    protected boolean registerPackageResult;

    /**
     * Gets the value of the registerPackageResult property.
     * 
     */
    public boolean isRegisterPackageResult() {
        return registerPackageResult;
    }

    /**
     * Sets the value of the registerPackageResult property.
     * 
     */
    public void setRegisterPackageResult(boolean value) {
        this.registerPackageResult = value;
    }

}
