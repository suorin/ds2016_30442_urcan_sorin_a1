
package packagesystem2;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the packagesystem2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: packagesystem2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdatePackageStatusResponse }
     * 
     */
    public UpdatePackageStatusResponse createUpdatePackageStatusResponse() {
        return new UpdatePackageStatusResponse();
    }

    /**
     * Create an instance of {@link CheckPackageStatusResponse }
     * 
     */
    public CheckPackageStatusResponse createCheckPackageStatusResponse() {
        return new CheckPackageStatusResponse();
    }

    /**
     * Create an instance of {@link UpdatePackageStatus }
     * 
     */
    public UpdatePackageStatus createUpdatePackageStatus() {
        return new UpdatePackageStatus();
    }

    /**
     * Create an instance of {@link CheckPackageStatus }
     * 
     */
    public CheckPackageStatus createCheckPackageStatus() {
        return new CheckPackageStatus();
    }

}
