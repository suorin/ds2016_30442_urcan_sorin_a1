
package packagesystem1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FindAllPackagesResult" type="{http://tempuri.org/}ArrayOfPackage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "findAllPackagesResult"
})
@XmlRootElement(name = "FindAllPackagesResponse")
public class FindAllPackagesResponse {

    @XmlElement(name = "FindAllPackagesResult")
    protected ArrayOfPackage findAllPackagesResult;

    /**
     * Gets the value of the findAllPackagesResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfPackage }
     *     
     */
    public ArrayOfPackage getFindAllPackagesResult() {
        return findAllPackagesResult;
    }

    /**
     * Sets the value of the findAllPackagesResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfPackage }
     *     
     */
    public void setFindAllPackagesResult(ArrayOfPackage value) {
        this.findAllPackagesResult = value;
    }

}
