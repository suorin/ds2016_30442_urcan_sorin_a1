package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	//PRODUCER
	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
			for (int i = 0; i < 3; i++) {
				DVD dvd = new DVD("Metro 203" + (i+3), 2012 + i, (double) (50 + i * 10));
				//DVD dvd = new DVD("dvd", 100, 100);
				String serializedObjectAsMessageContent = null;

				try {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ObjectOutputStream  oos = new ObjectOutputStream(baos);
					oos.writeObject(dvd);
					oos.flush();
					serializedObjectAsMessageContent = baos.toString();

					System.out.println("New DVD produced " + i);
				
				} catch (Exception e) {
				     System.out.println(e);
				}

				
				queue.writeMessage(serializedObjectAsMessageContent);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
