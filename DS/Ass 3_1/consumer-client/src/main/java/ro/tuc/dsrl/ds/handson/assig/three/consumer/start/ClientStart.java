package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.MailService;
import ro.tuc.dsrl.ds.handson.assig.three.queue.communication.DVD;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintWriter;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Consumer Client application. This application
 *  will run in an infinite loop and retrieve messages from the queue server
 *  and send e-mails with them as they come.
 */
public class ClientStart {

	//CONSUMER
	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

		MailService mailService = new MailService("sorin93u@gmail.com", "parola93");
		String message;

		while(true) {
			try {
				message = queue.readMessage();
				
				try {
					byte var[] = message.getBytes();
					ByteArrayInputStream bais = new ByteArrayInputStream(var);
					ObjectInputStream ois = new ObjectInputStream(bais);
					DVD dvd = (DVD) ois.readObject();
					
					System.out.println("Sending mail " + message);
					mailService.sendMail("sorin93u@gmail.com", "METRO new version available!", dvd.print());
					
				} catch (Exception e) {
					System.out.println(e);
				}
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
