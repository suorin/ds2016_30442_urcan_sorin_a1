package ro.tuc.dsrl.ds.handson.assig.three.queue.communication;

import java.io.Serializable;

public class DVD implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2031236045735149461L;
	
	private String title;
	private int year;
	private double price;
	
	public DVD(String string, int i, double d) {
		this.title = string;
		this.year = i;
		this.price = d;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String print() {
		String str = "Title: " + this.title + ", year: " + this.year + ", at only $" + this.price;
		return str;
	}
	
	
}
