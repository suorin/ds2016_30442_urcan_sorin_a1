﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ASS5
{
    public class Movie
    {
        [Display(Name = "Id")]
        public int Id
        {
            get;
            set;
        }

        [Display(Name = "Name")]
        public string Name
        {
            get;
            set;
        }

        [Display(Name = "Type")]
        public string Type
        {
            set;
            get;
        }
    }
    
}