﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using ASS5;

namespace ASS5_Client.Models
{
    public class UserServiceClient
    {
        private String BASE_URL = "http://localhost:52589/ServiceUser.svc/";


        public List<User> findAll()
        {
            try {
                var webClient = new WebClient();
                var json = webClient.DownloadString(BASE_URL + "findall");
                var js= new JavaScriptSerializer();
                return js.Deserialize<List<User>>(json);
            } catch {
                return null;
            }
        }

        public User find(string id)
        {
            try
            {
                var webClient = new WebClient();
                string url = string.Format(BASE_URL + "find/{0}", id);
                var json = webClient.DownloadString(url);
                var js = new JavaScriptSerializer();
                return js.Deserialize<User>(json);
            }
            catch
            {
                return null;
            }
        }

        public bool create(User user)
        {
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(User));
                MemoryStream mem = new MemoryStream();
                ser.WriteObject(mem, user);
                string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
                WebClient webClient = new WebClient();
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;
                webClient.UploadString(BASE_URL + "create", "POST", data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool edit(User user)
        {
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(User));
                MemoryStream mem = new MemoryStream();
                ser.WriteObject(mem, user);
                string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
                WebClient webClient = new WebClient();
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;
                webClient.UploadString(BASE_URL + "edit", "PUT", data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool delete(User user)
        {
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(User));
                MemoryStream mem = new MemoryStream();
                ser.WriteObject(mem, user);
                string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
                WebClient webClient = new WebClient();
                webClient.Headers["Content-type"] = "application/json";
                webClient.Encoding = Encoding.UTF8;
                webClient.UploadString(BASE_URL + "delete", "DELETE", data);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}