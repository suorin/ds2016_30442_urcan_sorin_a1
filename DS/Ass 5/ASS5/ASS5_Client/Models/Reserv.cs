﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ASS5
{
    public class Reserv
    {
        [Display(Name = "Id")]
        public int Id
        {
            get;
            set;
        }

        [Display(Name = "MovieID")]
        public int MovieID
        {
            get;
            set;
        }

        [Display(Name = "CinemaID")]
        public int CinemaID
        {
            get;
            set;
        }
    }
}