﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ASS5_Client.Models
{
    public class User
    {
        [Display(Name = "Id")]
        public int Id
        {
            get;
            set;
        }

        [Display(Name = "Username")]
        public string Username
        {
            get;
            set;
        }

        [Display(Name = "Password")]
        public string Password
        {
            get;
            set;
        }

        [Display(Name = "Type")]
        public string Type
        {
            get;
            set;
        }
    }
}