﻿using System.Web.Mvc;
using ASS5_Client.Models;
using ASS5_Client.ViewModels;

namespace ASS5_Client.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult ViewUser()
        {
            UserServiceClient usc = new UserServiceClient();
            ViewBag.listUsers = usc.findAll();
            return View();
        }

        // GET: User
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(UserViewModel pvm)
        {
            UserServiceClient usc = new UserServiceClient();
            usc.create(pvm.User);
            return RedirectToAction("ViewUser");
        }

        public ActionResult Delete(string id)
        {
            UserServiceClient usc = new UserServiceClient();
            usc.delete(usc.find(id));
            return RedirectToAction("ViewUser");
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            UserServiceClient usc = new UserServiceClient();
            UserViewModel uvm = new UserViewModel();
            uvm.User = usc.find(id);
            return View("Edit", uvm);
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel uvm)
        {
            UserServiceClient usc = new UserServiceClient();
            usc.edit(uvm.User);
            return RedirectToAction("ViewUser");
        }
    }
}