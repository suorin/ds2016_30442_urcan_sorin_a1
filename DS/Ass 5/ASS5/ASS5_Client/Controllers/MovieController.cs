﻿using System.Web.Mvc;
using ASS5_Client.Models;
using ASS5_Client.ViewModels;

namespace ASS5_Client.Controllers
{
    public class MovieController : Controller
    {
        // GET: Movie
        public ActionResult ViewMovie()
        {
            MovieServiceClient usc = new MovieServiceClient();
            ViewBag.listMovies = usc.findAll();
            return View();
        }

        // GET: Movie
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(MovieViewModel pvm)
        {
            MovieServiceClient usc = new MovieServiceClient();
            usc.create(pvm.Movie);
            return RedirectToAction("ViewMovie");
        }

        public ActionResult Delete(string id)
        {
            MovieServiceClient usc = new MovieServiceClient();
            usc.delete(usc.find(id));
            return RedirectToAction("ViewMovie");
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            MovieServiceClient usc = new MovieServiceClient();
            MovieViewModel uvm = new MovieViewModel();
            uvm.Movie = usc.find(id);
            return View("Edit", uvm);
        }

        [HttpPost]
        public ActionResult Edit(MovieViewModel uvm)
        {
            MovieServiceClient usc = new MovieServiceClient();
            usc.edit(uvm.Movie);
            return RedirectToAction("ViewMovie");
        }
    }
}