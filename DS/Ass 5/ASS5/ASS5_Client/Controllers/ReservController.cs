﻿using System.Web.Mvc;
using ASS5_Client.Models;
using ASS5_Client.ViewModels;

namespace ASS5_Client.Controllers
{
    public class ReservController : Controller
    {
        // GET: Reserv
        public ActionResult ViewUser()
        {
            ReservServiceClient usc = new ReservServiceClient();
            ViewBag.listReservs = usc.findAll();
            return View();
        }

        // GET: Reserv
        [HttpGet]
        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(ReservViewModel pvm)
        {
            ReservServiceClient usc = new ReservServiceClient();
            usc.create(pvm.Reserv);
            return RedirectToAction("ViewReserv");
        }

        public ActionResult Delete(string id)
        {
            ReservServiceClient usc = new ReservServiceClient();
            usc.delete(usc.find(id));
            return RedirectToAction("ViewReserv");
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            ReservServiceClient usc = new ReservServiceClient();
            ReservViewModel uvm = new ReservViewModel();
            uvm.Reserv = usc.find(id);
            return View("Edit", uvm);
        }

        [HttpPost]
        public ActionResult Edit(ReservViewModel uvm)
        {
            ReservServiceClient usc = new ReservServiceClient();
            usc.edit(uvm.Reserv);
            return RedirectToAction("ViewReserv");
        }
    }
}