﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ASS5
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceUser" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceUser.svc or ServiceUser.svc.cs at the Solution Explorer and start debugging.
    public class ServiceMovie : IServiceMovie
    {
        public bool create(Movie movie)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try {
                    movielist ul = new ASS5.movielist();
                    ul.id = movie.Id;
                    ul.name = movie.Name;
                    ul.type = movie.Type;
                    mde.movielists.Add(ul);
                    mde.SaveChanges();
                    return true;
                }
                catch {
                    return false;
                }
            };
        }

        public bool delete(Movie movie)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try
                {
                    int id = movie.Id;
                    movielist ul = mde.movielists.Single(p => p.id == id);
                    mde.movielists.Remove(ul);
                    mde.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            };
        }

        public bool edit(Movie movie)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try
                {
                    int id = movie.Id;
                    movielist ul = mde.movielists.Single(p => p.id == id);
                    ul.name = movie.Name;
                    ul.type = movie.Type;
                    mde.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            };
        }

        public Movie find(string id)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                int nid = Convert.ToInt32(id);
                return mde.movielists.Where(pe => pe.id == nid).Select(us => new Movie
                {
                    Id = us.id,
                    Name = us.name,
                    Type = us.type
                }).First();
            };
        }

        public List<Movie> findall()
        {
            using(ass5Entities mde = new ass5Entities())
            {
                return mde.movielists.Select(us => new Movie
                {
                    Id = us.id,
                    Name = us.name,
                    Type = us.type
                }).ToList();
            };
        }
    }
}
