﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASS5
{
    public class Cinema
    {

        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public ArraySegment<int> MoviesIDs
        {
            get;
            set;
        }
    }
}