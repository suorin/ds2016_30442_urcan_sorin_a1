﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASS5
{
    public class Movie
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Type
        {
            set;
            get;
        }
    }
    
}