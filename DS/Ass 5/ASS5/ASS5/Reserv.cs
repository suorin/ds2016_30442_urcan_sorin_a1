﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASS5
{
    public class Reserv
    {
        public int Id
        {
            get;
            set;
        }

        public int MovieID
        {
            get;
            set;
        }

        public int CinemaID
        {
            get;
            set;
        }
    }
}