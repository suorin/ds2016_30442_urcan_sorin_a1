﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ASS5
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceUser" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceUser.svc or ServiceUser.svc.cs at the Solution Explorer and start debugging.
    public class ServiceUser : IServiceUser
    {
        public bool create(User user)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try {
                    userlist ul = new ASS5.userlist();
                    ul.username = user.Username;
                    ul.password = user.Password;
                    ul.type = user.Type;
                    mde.userlists.Add(ul);
                    mde.SaveChanges();
                    return true;
                }
                catch {
                    return false;
                }
            };
        }

        public bool delete(User user)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try
                {
                    int id = user.Id;
                    userlist ul = mde.userlists.Single(p => p.id == id);
                    mde.userlists.Remove(ul);
                    mde.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            };
        }

        public bool edit(User user)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try
                {
                    int id = user.Id;
                    userlist ul = mde.userlists.Single(p => p.id == id);
                    ul.username = user.Username;
                    ul.password = user.Password;
                    ul.type = user.Type;
                    mde.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            };
        }

        public User find(string id)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                int nid = Convert.ToInt32(id);
                return mde.userlists.Where(pe => pe.id == nid).Select(us => new User
                {
                    Id = us.id,
                    Username = us.username,
                    Password = us.password,
                    Type = us.type
                }).First();
            };
        }

        public List<User> findall()
        {
            using(ass5Entities mde = new ass5Entities())
            {
                return mde.userlists.Select(us => new User
                {
                    Id = us.id,
                    Username = us.username,
                    Password = us.password,
                    Type = us.type
                }).ToList();
            };
        }
    }
}
