﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ASS5
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceReserv" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceReserv.svc or ServiceReserv.svc.cs at the Solution Explorer and start debugging.
    public class ServiceReserv : IServiceReserv
    {
        public bool create(Reserv user)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try {
                    reservation ul = new ASS5.reservation();
                    ul.movieID = user.MovieID;
                    ul.cinemaID = user.CinemaID;
                    mde.reservations.Add(ul);
                    mde.SaveChanges();
                    return true;
                }
                catch {
                    return false;
                }
            };
        }

        public bool delete(Reserv user)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try
                {
                    int id = user.Id;
                    reservation ul = mde.reservations.Single(p => p.id == id);
                    mde.reservations.Remove(ul);
                    mde.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            };
        }

        public bool edit(Reserv user)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                try
                {
                    int id = user.Id;
                    reservation ul = mde.reservations.Single(p => p.id == id);
                    ul.movieID = user.MovieID;
                    ul.cinemaID = user.CinemaID;
                    mde.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            };
        }

        public Reserv find(string id)
        {
            using (ass5Entities mde = new ass5Entities())
            {
                int nid = Convert.ToInt32(id);
                return mde.reservations.Where(pe => pe.id == nid).Select(us => new Reserv
                {
                    Id = us.id,
                    MovieID = us.movieID,
                    CinemaID = us.cinemaID
                }).First();
            };
        }

        public List<Reserv> findall()
        {
            using(ass5Entities mde = new ass5Entities())
            {
                return mde.reservations.Select(us => new Reserv
                {
                    Id = us.id,
                    MovieID = us.movieID,
                    CinemaID = us.cinemaID
                }).ToList();
            };
        }
    }
}
