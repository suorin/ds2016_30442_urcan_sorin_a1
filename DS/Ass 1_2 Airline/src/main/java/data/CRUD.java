package data;

import model.Flight;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class CRUD {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration
				.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public static Integer create(Flight e) {
		Session session = getSessionFactory().openSession();
		session.beginTransaction();
		session.save(e);
		session.getTransaction().commit();
		session.close();
		System.out.println("Successfully created" + e.toString());
		return e.getId();

	}

	public static List<Flight> read() {
		Session session = getSessionFactory().openSession();
		@SuppressWarnings("unchecked")
		List<Flight> flights = session.createQuery("FROM Flight").list();
		session.close();
		System.out.println("Found " + flights.size() + " Flights");
		return flights;

	}

	public static void update(Flight e) {
		Session session = getSessionFactory().openSession();
		session.beginTransaction();
		Flight em = (Flight) session.load(Flight.class, e.getId());
		em.setAirplaneType(e.getAirplaneType());
		em.setArrival(e.getArrival());
		em.setArrivalCity(e.getArrivalCity());
		em.setDeparture(e.getDeparture());
		em.setDepartureCity(e.getDepartureCity());
		
		session.getTransaction().commit();
		session.close();
		System.out.println("Successfully updated " + e.toString());

	}

	public static void delete(Integer id) {
		Session session = getSessionFactory().openSession();
		session.beginTransaction();
		Flight e = findByID(id);
		session.delete(e);
		session.getTransaction().commit();
		session.close();
		System.out.println("Successfully deleted " + e.toString());

	}

	public static Flight findByID(Integer id) {
		Flight tmp= null;
		Session session = getSessionFactory().openSession();
		List<Flight> flights = session.createQuery("FROM Flight").list();
		for(Flight flightWithId: flights){
			if(flightWithId.getId() == id){
				tmp = flightWithId;
			}
		}
		//Flight e = (Flight) session.load(Flight.class, id);
		session.close();
		return tmp;
	}
	
	public static void deleteAll() {
		Session session = getSessionFactory().openSession();
		session.beginTransaction();
		Query query = session.createQuery("DELETE FROM Flight ");
		query.executeUpdate();
		session.getTransaction().commit();
		session.close();
		System.out.println("Successfully deleted all flights.");

	}
}

