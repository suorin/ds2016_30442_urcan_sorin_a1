package data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import model.Flight;

public class FlightDB {
	
	
	static final long ONE_MINUTE_IN_MILLIS = 60000;
	List<Flight> flights = new ArrayList<Flight>();
	String[] citiesDeparture = { "Cluj", "Singapore", "Bangladesh", "Turin", "Berlin" };
	String[] citiesArrival = { "Londra", "Vaslui", "Sibiu", "Karlsruhe", "Turda" };
	String[] airPlanes = { "wizz", "tarom", "carpati", "funingine", "funingine2" };
	Random rand = new Random();
	// Create an instance of SimpleDateFormat used for formatting
	// the string representation of date (month/day/year)
	DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	// Get the date today using Calendar object.
	Date today = Calendar.getInstance().getTime();
	// Using DateFormat format method we can create a string
	// representation of a date with the defined format.
	String reportDate = df.format(today);

	private void createFlights() {
		int citiesI;
		int planesI;
		int planesArrivalI;
		int planesDepartureI;
		for (int i = 0; i < 5; i++) {
			// Create an instance of SimpleDateFormat used for formatting
			// the string representation of date (month/day/year)
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			// Get the date today using Calendar object.
			Date today = Calendar.getInstance().getTime();
			long currentTime = today.getTime();
			planesDepartureI = rand.nextInt(1800) + 1;
			planesArrivalI = rand.nextInt(1800) + 1;
			Date todayTimeDeparture = new Date(currentTime + (planesDepartureI * ONE_MINUTE_IN_MILLIS));
			Date todayTimeArrival = new Date(
					currentTime + ((planesDepartureI + planesArrivalI) * ONE_MINUTE_IN_MILLIS));

			// Using DateFormat format method we can create a string
			// representation of a date with the defined format.
			String reportDate = df.format(todayTimeDeparture);
			String reportDateArrival = df.format(todayTimeArrival);
			citiesI = rand.nextInt(5);// check usage!
			planesI = rand.nextInt(3);
			// Flight f = new Flight(cities[i], cities[j]);
			Flight f = new Flight(i, airPlanes[planesI], citiesDeparture[citiesI], reportDate, citiesArrival[citiesI], reportDateArrival);

			flights.add(f);
		}
	}

	public void addFlightToList(Flight e) {
		flights.add(e);
	}

	public void removeFlightFromList(int id) {
		Flight tmp=null;
		for (Flight f : flights) {
			if (f.getId() == id) {
				tmp = f;
			}
		}
		flights.remove(tmp);
	}

	public FlightDB() {
		//createFlights();
	}

	public List<Flight> getFlights() {
		return flights;
	}

	public Flight findFlightsWithDestination(String city) {
		Flight tmp = new Flight();
		for (Flight f : flights) {
			if (f.getDestination(city)) {
				tmp = f;
			}

		}
		return tmp;
	}

}