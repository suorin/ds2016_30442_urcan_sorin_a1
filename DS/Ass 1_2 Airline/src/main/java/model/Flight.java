package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "flights")
public class Flight {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private int id=0;
	
	@Column(name="airplaneType")
	private String airplaneType;
	
	@Column(name="departure")
	private String departure;
	
	@Column(name="arrivalCity")
	private String arrivalCity;
	
	@Column(name="arrival")
	private String arrival;
	
	@Column(name="departureCity")
	private String departureCity;
	
	public Flight(){
		
	}
	
	public Flight(int number, String airplaneType,String departureCity ,String departure,String arrivalCity,String arrival) {
		this.id = number;
		this.airplaneType = airplaneType;
		this.departure = departure;
		this.arrivalCity= arrivalCity;
		this.arrival = arrival;
		this.departureCity=departureCity;
	}

	
	public boolean getDestination(String arrivalCity){
		if(this.getArrivalCity().equals(arrivalCity)){
			return true;
		}
		else {
			System.out.println("No destination");
			return false;
		}
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int number) {
		this.id = number;
	}

	public String getAirplaneType() {
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType) {
		this.airplaneType = airplaneType;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}
	@Override
	public String toString(){
		return "Flight: " + this.id + ", " +this.airplaneType+ ", " +this.arrival+
				", "+this.arrivalCity + ", "+ this.departure+ ", " + this.departureCity;
	}
	
}
