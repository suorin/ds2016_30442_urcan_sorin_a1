package view;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.FlightDB;

@WebServlet(name="adminAction", urlPatterns ="/admin")
public class AdminServlet extends HttpServlet implements Servlet {
	
FlightDB fDB = new FlightDB();	
@Override

protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	// TODO Auto-generated method stub
	super.doGet(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	RequestDispatcher view = req.getRequestDispatcher("/SuperAdmin.jsp");
	fDB = new FlightDB();
	
	req.setAttribute("flightList",fDB.getFlights());
	view.forward(req, resp);
	}
}
