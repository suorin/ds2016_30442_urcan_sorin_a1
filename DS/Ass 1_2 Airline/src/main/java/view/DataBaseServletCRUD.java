package view;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.CRUD;
import data.FlightDB;
import model.Flight;

@WebServlet(name = "DBServletCrud", urlPatterns = "/DBServletCrud")
public class DataBaseServletCRUD extends HttpServlet implements Servlet {

	FlightDB flightDB = new FlightDB();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Flight flight = new Flight();

	String airplaneType = req.getParameter("airplaneType");
	String departureCity = req.getParameter("departureCity");
	String departure = req.getParameter("departure");
	String arrivalCity = req.getParameter("arrivalCity");
	String arrival = req.getParameter("arrival");
		
		flight.setId(0);
		flight.setAirplaneType(airplaneType);
		flight.setDepartureCity(departureCity);
		flight.setDeparture(departure);
		flight.setArrivalCity(arrivalCity);
		flight.setArrival(arrival);

		flightDB.addFlightToList(flight);
	


		RequestDispatcher requestDispatcher = req.getRequestDispatcher("/SuperAdmin.jsp");
		req.setAttribute("flightList", flightDB.getFlights());
		requestDispatcher.forward(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String getId = req.getParameter("id");
		int flightId = Integer.parseInt(getId);
		if (req.getParameter("isSearch")!=null && req.getParameter("isSearch").equals("true")) {

			ArrayList<Flight> flights = new ArrayList<Flight>();
			flights.add(CRUD.findByID(flightId));

			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/user.jsp");
			req.setAttribute("flightList", flights);
			requestDispatcher.forward(req, resp);
		} 
		
		else if(req.getParameter("isUpdate")!=null && req.getParameter("isUpdate").equals("true")){
			Flight tmp= new Flight();
			ArrayList<Flight> flights = new ArrayList<Flight>();
			tmp = CRUD.findByID(flightId);
			String airplaneType = req.getParameter("airplaneType");
			String departureCity = req.getParameter("departureCity");
			String departure = req.getParameter("departure");
			String arrivalCity = req.getParameter("arrivalCity");
			String arrival = req.getParameter("arrival");
			
			System.out.println("AM INTRAT IN UPDATE");
			tmp.setAirplaneType(airplaneType);
			tmp.setDepartureCity(departureCity);
			tmp.setDeparture(departure);
			tmp.setArrivalCity(arrivalCity);
			tmp.setArrival(arrival);
			
			
			
			CRUD.update(tmp);
			flights.add(tmp);
			flightDB.addFlightToList(tmp);
			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/SuperAdmin.jsp");
			req.setAttribute("flightList", flights);
			requestDispatcher.forward(req, resp);
			
		}
		else {
			CRUD.delete(flightId);
			flightDB.removeFlightFromList(flightId);
			RequestDispatcher requestDispatcher = req.getRequestDispatcher("/SuperAdmin.jsp");
			req.setAttribute("flightList", flightDB.getFlights());
			requestDispatcher.forward(req, resp);
		}
	}

}
