package view;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import data.CRUD;
import data.FlightDB;

@WebServlet(name="UserAction", urlPatterns ="/user")
public class UserServlet extends HttpServlet implements Servlet {
@Override
protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	// TODO Auto-generated method stub
	super.doGet(req, resp);
}
@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

	RequestDispatcher view = req.getRequestDispatcher("/user.jsp");
	req.setAttribute("flightList",CRUD.read());
	view.forward(req, resp);
	}

}
