<%@page import= "java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="javassist.expr.NewArray"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="model.Flight" %>
    <%@ page import="data.CRUD" %>
    <%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="Airline/css/bootstrap.min.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body>

Today's date: <%= (new java.util.Date()).toLocaleString()%>

<%Flight flights = new Flight();%>
<%CRUD crudOperation = new CRUD(); %>
 <%
 List<Flight> flightList = (List<Flight>)request.getAttribute ("flightList");
 //List<Flight> flightList = new ArrayList<Flight>();
%>

<form action="DBServletCrud" method="post">
airplaneType: <input type="text" id="airplaneType" name="airplaneType" value="airplaneType"><br>
departureCity:<input type="text" id="departureCity" name="departureCity" value="departureCity"><br>
departure :<input type="text" id="departure" name="departure" value="departure"><br>
arrival:<input type="text" id="arrival" name="arrival" value="arrival"><br>
arrivalCity:<input type="text" id="arrivalCity" name="arrivalCity" value="arrivalCity"><br>
<input type="submit" value="Create"><br>
</form>


<form action="DBServletCrud" method="get">
id:<input type="text" name="id" value="id">
<input type="hidden" name="isDeleted" value="true" >
<input type="submit" value="Delete"><br>
</form> 

<form action="DBServletCrud" method="get">
UpdateById:<input type="text" name="id" value="id"><br>
airplaneType: <input type="text" id="airplaneType" name="airplaneType" value="airplaneType"><br>
departureCity:<input type="text" id="departureCity" name="departureCity" value="departureCity"><br>
departure :<input type="text" id="departure" name="departure" value="departure"><br>
arrival:<input type="text" id="arrival" name="arrival" value="arrival"><br>
arrivalCity:<input type="text" id="arrivalCity" name="arrivalCity" value="arrivalCity"><br>
<input type="hidden" name="isUpdate" value="true" >
<input type="submit" value="Update"><br>
</form>

<%/*
String airplaneType = request.getParameter("airplaneType");
String departureCity = request.getParameter("departureCity");
String departure = request.getParameter("departure");
String arrivalCity = request.getParameter("arrivalCity");
String arrival = request.getParameter("arrival");

flights.setAirplaneType(airplaneType);
flights.setDepartureCity(departureCity);
flights.setDeparture(departure);
flights.setArrivalCity(arrivalCity);
flights.setArrival(arrival);
flightList.add(flights);*/
%>



<table>
  <tr>
    <th>id</th>
    <th>airplaneType</th>
    <th>departureCity</th>
    <th>departure</th>
    <th>arrivalCity</th>
    <th>arrival</th>
  </tr>
  <% for(Flight flight:flightList) { %>
        <tr>
        <td><%=flight.getId()%></td>
    	<td><%=flight.getAirplaneType()%></td>
    	<td><%=flight.getDepartureCity()%></td>
    	<td><%=flight.getDeparture()%></td>
    	<td><%=flight.getArrivalCity()%></td>
    	<td><%=flight.getArrival()%></td>  
        </tr>
    <% } %>
</table>

</body>
</html>