package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-server
 * @Since: Sep 1, 2015
 * @Description:
 * 	Class used for computation of taxes to be paid for a specific car. An instance
 * 	of this class is published in the Registry so that it can be remotely accessed
 * 	by a client.
 */
public class TaxService implements ITaxService {

	public double computeTax(Car c) {
		// Dummy formula
		if (c.getEngineSize() <= 0) {
			throw new IllegalArgumentException("Engine size must be positive.");
		}
		int sum = 8;
		if(c.getEngineSize() > 1600) sum = 18;
		if(c.getEngineSize() > 2000) sum = 72;
		if(c.getEngineSize() > 2600) sum = 144;
		if(c.getEngineSize() > 3001) sum = 290;
		return c.getEngineSize() / 200.0 * sum;
	}

	public double computeSellingPrice(Car c) {
		// TODO Auto-generated method stub
		if (c.getPrice() < 0) {
			throw new IllegalArgumentException("Purchase price must be positive.");
		}
		if(c.getYear() < 1769 ) {
			throw new IllegalArgumentException("The year of production is not real.");
		}
		return c.getPrice() - ((c.getPrice() / 7) * (2015 - c.getYear()));
		
	}
}
