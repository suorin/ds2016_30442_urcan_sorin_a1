package controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ro.tuc.dsrl.ds.handson.assig.two.client.communication.ServerConnection;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.IPriceService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;
import view.ClientView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *  Controller for the interface elements of the client.
 */
public class ViewController {
	private static final Log LOGGER = LogFactory.getLog(ViewController.class);

	private static final String ERROR_MESSAGE =
			"An exception occured while trying to send data to server. Please consult logs for more info!";
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientView clientView;
	//private ServerConnection serverConnection;

	public ViewController() {
		clientView = new ClientView();
		clientView.setVisible(true);

		//serverConnection = new ServerConnection(HOST, PORT);

		clientView.addBtnGetActionListener(new GetActionListener());
		clientView.addBtnDeleteActionListener(new DeleteActionListener());
		clientView.addBtnPostActionListener(new PostActionListener());
	}

	public void printMessage(String message) {
		clientView.printString(message);
		
	}
	
	private void clearWindow() {
		clientView.clearTextArea();
		
	}

	public void displayErrorMessage(String message) {
		clientView.clear();
		JOptionPane.showMessageDialog(clientView, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void displayInfoMessage(String message) {
		clientView.clear();
		JOptionPane.showMessageDialog(clientView, message, "Success", JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Provides functionality for the POST button.
	 */
	class PostActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String year = clientView.getYear();
			String size = clientView.getEngineSize();
			//String price = clientView.getPrice();

			if (!("".equals(year) || "".equals(size))) {
				try {
					ITaxService taxService = null;
					taxService = Naming.lookup(ITaxService.class, ServerConnection.getInstance());

					printMessage("Tax value: " + taxService.computeTax(
							new Car(Integer.parseInt(year), Integer.parseInt(size))));

				} catch (Exception el) {
					LOGGER.error("", el);
					
				}
			}
			else {
				displayErrorMessage("Please fill all textboxes before submiting!");
			}
		}
	}

	/**
	 * Provides functionality for the GET button.
	 */
	class GetActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String year = clientView.getYear();
			String size = clientView.getEngineSize();
			String price = clientView.getPrice();

			
			if (!("".equals(year) || "".equals(size) || "".equals(price))) {
				try {
					IPriceService priceService = null;
					priceService = Naming.lookup(IPriceService.class, ServerConnection.getInstance());

					printMessage("Selling price: " + priceService.computePrice(
							new Car(Integer.parseInt(year), Integer.parseInt(size), Integer.parseInt(price))));
				} catch (Exception el) {
					LOGGER.error("", el);
					
				}
			}
			else {
				displayErrorMessage("Please fill all textboxes before submiting!");
			}
		}
	}
	
	
	/**
	 * Provides functionality for the Delete button.
	 */
	class DeleteActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			clearWindow();
		}

	}
	
	
	
}
