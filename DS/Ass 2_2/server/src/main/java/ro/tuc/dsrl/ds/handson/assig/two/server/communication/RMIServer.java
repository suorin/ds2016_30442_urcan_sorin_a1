package ro.tuc.dsrl.ds.handson.assig.two.server.communication;

import java.nio.channels.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;


import rmi.Constant;
import ro.tuc.dsrl.ds.handson.assig.two.server.services.TaxService;

public class RMIServer {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException, java.rmi.AlreadyBoundException {
		Remote remote = new TaxService();
		Registry registry = LocateRegistry.createRegistry(Constant.RMI_PORT);
		registry.bind(Constant.RMI_ID, remote);
		System.out.println("Server has started!");
	}

}
