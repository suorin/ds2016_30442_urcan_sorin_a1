package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import controllers.ViewController;
import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-client
 * @Since: Sep 24, 2015
 * @Description: Starting point of the Client application.
 */
public class ClientStart {
	
	private ClientStart() {
	}

	public static void main(String[] args) throws IOException {
	
		new ViewController();
	}
}
