package controllers;

import rmi.Constant;
import rmi.RMIRemote;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import view.ClientView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *  Controller for the interface elements of the client.
 */
public class ViewController {
	private ClientView clientView;

	public ViewController() {
		clientView = new ClientView();
		clientView.setVisible(true);

		clientView.addBtnGetActionListener(new GetActionListener());
		clientView.addBtnDeleteActionListener(new DeleteActionListener());
		clientView.addBtnPostActionListener(new PostActionListener());
	}

	public void printMessage(String message) {
		clientView.printString(message);
		
	}
	
	private void clearWindow() {
		clientView.clearTextArea();
		
	}

	public void displayErrorMessage(String message) {
		clientView.clear();
		JOptionPane.showMessageDialog(clientView, message, "Error", JOptionPane.ERROR_MESSAGE);
	}

	public void displayInfoMessage(String message) {
		clientView.clear();
		JOptionPane.showMessageDialog(clientView, message, "Success", JOptionPane.PLAIN_MESSAGE);
	}

	/**
	 * Provides functionality for the POST button.
	 */
	class PostActionListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String year = clientView.getYear();
			String size = clientView.getEngineSize();

			if (!("".equals(year) || "".equals(size))) {
				
				Registry registry;
				try {
					registry = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT);
					RMIRemote remote = (RMIRemote) registry.lookup(Constant.RMI_ID);
					
					printMessage("Tax value: " + remote.computeTax(new Car(Integer.parseInt(year), Integer.parseInt(size))));
					
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			else {
				displayErrorMessage("Please fill all textboxes before submiting!");
			}
		}
	}

	/**
	 * Provides functionality for the GET button.
	 */
	class GetActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String year = clientView.getYear();
			String size = clientView.getEngineSize();
			String price = clientView.getPrice();

			
			if (!("".equals(year) || "".equals(size) || "".equals(price))) {
				
				Registry registry;
				try {
					registry = LocateRegistry.getRegistry("localhost", Constant.RMI_PORT);
					RMIRemote remote = (RMIRemote) registry.lookup(Constant.RMI_ID);
					printMessage("Selling price: " + remote.computePrice(
							new Car(Integer.parseInt(year), Integer.parseInt(size), Integer.parseInt(price))));
					
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (NotBoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
			else {
				displayErrorMessage("Please fill all textboxes before submiting!");
			}
		}
	}
	
	
	/**
	 * Provides functionality for the Delete button.
	 */
	class DeleteActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			clearWindow();
		}

	}
	
	
	
}
