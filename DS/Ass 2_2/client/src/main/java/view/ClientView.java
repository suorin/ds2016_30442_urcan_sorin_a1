package view;


import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	CatalogView is a JFrame which contains the UI elements of the Client application.
 */


public class ClientView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textyear;
	private JTextField textSize;
	private JTextField textPrice;
	//private JTextField textId;
	private JButton btnPrice;
	private JButton btnGetTax;
	private JButton btnClear;
	private JTextArea textArea;

	public ClientView() {
		setTitle("HTTP Protocol simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 270, 430);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertNewCar = new JLabel("Insert car details below:");
		lblInsertNewCar.setBounds(10, 11, 150, 14);
		contentPane.add(lblInsertNewCar);

		JLabel lblYear = new JLabel("Fabrication year");
		lblYear.setBounds(10, 36, 110, 14);
		contentPane.add(lblYear);

		JLabel lblSize = new JLabel("Engine size");
		lblSize.setBounds(10, 61, 110, 14);
		contentPane.add(lblSize);

		JLabel lblPrice = new JLabel("Purchasing price");
		lblPrice.setBounds(10, 86, 110, 14);
		contentPane.add(lblPrice);

		textyear = new JTextField();
		textyear.setBounds(120, 33, 86, 20);
		contentPane.add(textyear);
		textyear.setColumns(10);

		textSize = new JTextField();
		textSize.setBounds(120, 58, 86, 20);
		contentPane.add(textSize);
		textSize.setColumns(10);

		textPrice = new JTextField();
		textPrice.setBounds(120, 83, 86, 20);
		contentPane.add(textPrice);
		textPrice.setColumns(10);

		btnGetTax = new JButton("Get Tax");
		btnGetTax.setBounds(10, 120, 89, 23);
		contentPane.add(btnGetTax);
		
		//btnGetTax = new JButton("Get selling price");
		//btnGetTax.setBounds(120, 120, 130, 23);
		//contentPane.add(btnGetTax);

		//JLabel lblFindStudentBy = new JLabel("Find student by id ");
		//lblFindStudentBy.setBounds(235, 11, 145, 14);
		//contentPane.add(lblFindStudentBy);

		//JLabel lblId = new JLabel("Id");
		//lblId.setBounds(235, 36, 46, 14);
		//contentPane.add(lblId);

		//textId = new JTextField();
		//textId.setBounds(320, 33, 86, 20);
		//contentPane.add(textId);
		//textId.setColumns(10);

		btnPrice = new JButton("Get selling price");
		btnPrice.setBounds(105, 120, 130, 23);
		contentPane.add(btnPrice);
		
		textArea = new JTextArea();
		textArea.setBounds(20, 160, 215, 175);
		contentPane.add(textArea);
	    
		btnClear = new JButton("Clear");
		btnClear.setBounds(140, 350, 89, 23);
		contentPane.add(btnClear);
	}

	public void addBtnGetActionListener(ActionListener e) {
		btnPrice.addActionListener(e);
	}

	public void addBtnPostActionListener(ActionListener e) {
		btnGetTax.addActionListener(e);
	}
	
	public void addBtnDeleteActionListener(ActionListener e) {
		btnClear.addActionListener(e);
	}

	public void clearTextArea() {
		textArea.setText("");
	}

	public String getYear() {
		return textyear.getText();
	}

	public String getEngineSize() {
		return textSize.getText();
	}

	public String getPrice() {
		return textPrice.getText();
	}

	public void printString(String message) {
		textArea.append(message + "\n");
	}

	public void clear() {
		textyear.setText("");
		textSize.setText("");
		textPrice.setText("");
	}
	

}
