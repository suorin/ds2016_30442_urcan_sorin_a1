package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

public interface RMIRemote extends Remote {
	public double computeTax(Car c) throws RemoteException;
	public double computePrice(Car c) throws RemoteException;
}
